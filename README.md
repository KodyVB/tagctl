# Table of Contents

[[_TOC_]]

# Synopsis

Tagctl is a command line program written in Rust which can add or remove tags to files.
The tags can either be in the name or under `user.xdg.tags` in the extended attributes.

# Overview

Tagctl can take multiple files as input from both the left hand side (LHS) and right hand side (RHS).
It breaks the inputs up by new lines, using each line as a new file.
Before altering the file, it checks to make sure the file does exist, and if it doesn't exist tagctl will move on to the next file.
For instructions, you can run `tagctl -h`, and for more verbose instructions you can run `tagctl --help`.

# Installation

Tagctl can either be built from scratch via Rust or the pre-compiled binary can be used.

## Build from Scratch

1. Install and set up Rust preferably via rustup (Arch wiki instructions [here](https://wiki.archlinux.org/title/rust#Rustup))
2. Run `cargo install tagctl`

## Pre-built Binary

1. Download the tagctl binary (found [in this Mega.nz link](https://mega.nz/folder/ngYUyKQS#Zco8wR9__Y5VUbYR1pT-SQ)) which was compiled under cargo's `--target x86_64-unknown-linux-musl` flag for general Linux use
2. If it didn't download as an executable, make it executable with `chmod +x path/to/tagctl`
3. Move binary to somewhere in your PATH environment or use by specifying its path

## Tab Completion

Once installed, tagctl can generate the completion files via the `-g` flag for Bash, Elvish, Fish, and Zsh.
To utilize the script, do the step relevant to your shell:

- Bash: Generate the `tagctl.bash` file to your `bash_completion.d` or `bash-completion/completions` directory, or `source` it otherwise
  - e.g. `tagctl -g bash > $XDG_DATA_HOME/bash-completion/completions/tagctl.bash`
- Elvish: Generate the `tagctl.elv` file to a [module search directory](https://elv.sh/ref/command.html#module-search-directories), such as `$XDG_CONFIG_HOME/elvish/lib`, and add `use tagctl` to your `rc.elv` file
  - e.g. `tagctl -g elvish > $XDG_CONFIG_HOME/elvish/lib/tagctl.elv`
- Fish: Generate the `tagctl.fish` file to `$XDG_CONFIG_HOME/fish/completions`
  - e.g. `tagctl -g fish > $XDG_CONFIG_HOME/fish/completions/tagctl.fish`
- Zsh: Generate the `_tagctl` file to somewhere in your `$fpath`, preferably creating a custom `$fpath` directory by doing something like `mkdir -pv $XDG_CONFIG_HOME/zsh/completions && tagctl -g zsh > $XDG_CONFIG_HOME/zsh/completions/_tagctl`, then adding the following to your `zshrc`:

```zsh
fpath=(${XDG_CONFIG_HOME}/zsh/completions "${fpath[@]}")
autoload -Uz compinit
compinit -u
```

# Tags

Tags can be specified as a normal string or one of the following escaped sequences:

- `%p` - the file's parent directory's name
- `%y` - the year the file was modified
- `%m` - the month the file was modified
- `%d` - the day the file was modified
- `%w` - the weekday the file was modified

## Adding Tags

In order to specify the tags, use the `-t` or `--tag` flag.
For example, in order to add the tag "this example" to the file "a.txt", the following would e used:

```sh
$ ls
a.txt
$ tagctl -t "this example" a.txt
$ ls
'a[this example].txt'
```

## Removing Tags

In order to remove a tag, add the `-r` or `--remove` flag to the command.
For example, to remove the tag "this example" from the file "a.txt", the following would be used:

```sh
$ ls
'a[this example].txt'
$ tagctl -rt "this example" a\[this\ example\].txt
$ ls
a.txt
```

You could also remove all of the tags from a file using the `-R` or `--remove_all` flag.
For example, to remove all of the tags from "a.txt", the following could be used:

```sh
$ ls
'a[this example,another example].txt'
$ tagctl -R a\[this\ example,another\ example\].txt
$ ls
a.txt
```

## Changing Delimiters

If you don't want to use the default delimiter of "," to separate multiple tags, use the `-d` or `--delimiter` flag.
It is left to the user to stay consistent with delimiters - if the user adds the tags "first", "second", and "third" to a file with the default delimiter of "," and then uses the delimiter flag to change the delimiter to something else, tagctl will see "first,second,third" as a single tag.
An example of the delimiter flag being used is shown here:

```sh
$ ls
a.txt
$ tagctl -t "first" -d "-" a.txt
$ ls
'a[first].txt'
$ tagctl -t "second" -d "-" a\[first\].txt
$ ls
'a[first-second].txt'
```

## Extended Attributes

Tagctl has the option to change the tags in extended attributes, which can be accessed via the command line with programs such as `getfattr` and `setfattr`.
It uses the standard that KDE's file indexer, [Baloo](https://community.kde.org/Baloo), uses, being `user.xdg.tags`.
In order to use the extended attributes, add the `-x` or `--xattr` flag to the command.
For example, to add the tag "picture" to the file "a.jpg", the following would be used:

```sh
$ ls
a.jpg
$ tagctl -xt "picture" a.jpg
$ ls
a.jpg
$ getfattr -n user.xdg.tags a.jpg
# file: a.jpg
user.xdg.tags="picture"
$ baloosearch tag:picture
...
/path/to/a.jpg
...
```

## LHS Arguments

Tagctl optionally takes in file names from the left hand side via pipes via the `-i` and `--input` flags.
Some examples are shown here:

```sh
$ find . -type f | tagctl -it "tagName"
$ ls | tagctl -xivt "anotherTagName"
$ ls > file_list.txt
$ cat file_list.txt | tagctl -xivrt "anotherTagName"
```
