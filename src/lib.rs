/*
Copyright (C) 2022  Kody VB
This file is part of Tagctl.

Tagctl is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Tagctl is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
Tagctl. If not, see <https://www.gnu.org/licenses/>.
*/

use core::panic;
use std::{
    io,
    path::{Path, PathBuf},
    process::exit,
};

use clap_complete::generate;

mod cli;
mod name_logic;
mod shared_logic;
mod xattr_logic;

#[must_use]
pub fn get_args() -> cli::Args {
    cli::Args::from(cli::build().get_matches())
}

/// # Panics
///
/// Will panic if auto-complete is chosen, invalid option is chosen, and Clap doesn't catch it
pub fn run(args: &cli::Args) {
    let desired_action = |path: &Path, args: &cli::Args| match args.action {
        cli::DesiredAction::GenerateAutocomplete => {}
        cli::DesiredAction::AddTag => xattr_logic::x_add_tag(path, args),
        cli::DesiredAction::RemoveTag => xattr_logic::x_remove_tag(path, args),
        cli::DesiredAction::RemoveAllTag => xattr_logic::x_remove_all_tags(path, args),
        cli::DesiredAction::AddName => name_logic::add_tag(path, args),
        cli::DesiredAction::RemoveName => name_logic::remove_tag(path, args),
        cli::DesiredAction::RemoveAllName => name_logic::remove_all_tags(path, args),
    };

    if args.action == cli::DesiredAction::GenerateAutocomplete {
        generate(
            args.autocomplete_created()
                .unwrap_or_else(|| panic!("Error: Uncaught incorrect shell script selected")),
            &mut cli::build(),
            "tagctl",
            &mut io::stdout(),
        );
        exit(0)
    }

    let paths: Vec<PathBuf> = args.get_paths();
    if paths.is_empty() {
        eprintln!("Error: No valid paths given");
        return;
    }

    for path in paths {
        desired_action(&path, args);
    }
}
